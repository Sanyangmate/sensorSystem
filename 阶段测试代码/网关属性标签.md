土壤湿度60cm
soilhumi60cm
float 取值范围：0 ~ 100 0.01

土壤湿度40cm
soilhumi40cm
float 取值范围：0 ~ 100 0.01

土壤湿度20cm
soilhumi20cm
float 取值范围：0 ~ 100 0.01


土壤温度60cm
soiltemp60cm
float 取值范围：-40 ~ 80  0.01

土壤温度40cm
soiltemp40cm
float 取值范围：-40 ~ 80

土壤温度20cm
soiltemp20cm
float 取值范围：-40 ~ 80


含钾量
soilK
int32 (整数型)取值范围：0 ~ 500 1

含磷量
soilP
int32 (整数型)取值范围：0 ~ 500 1

含氮量
soilN
int32 (整数型)取值范围：0 ~ 500 1

土壤EC值
soilEC
int32 (整数型)取值范围：0 ~ 20000 1

土壤PH
soilPH
float (单精度浮点型)取值范围：0 ~ 15 0.01


地理位置
GeoLocationA
struct (结构体) 





